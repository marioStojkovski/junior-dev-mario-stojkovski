<?php

namespace ProductStoreApp\ProductStore\Products\Furniture;
use ProductStoreApp\ProductStore\Products\Products;



class Furniture extends Products {
    
    public function productToSetAsObject($sku, $name, $price, $productType, $specialAttributeArray){
        $this->sku = filter_var($sku, FILTER_SANITIZE_STRING);
        $this->name = filter_var($name, FILTER_SANITIZE_STRING);
        $this->price = filter_var($price, FILTER_SANITIZE_STRING);
        $this->productType = filter_var($productType, FILTER_SANITIZE_STRING);
        $this->specialAttribute = filter_var($specialAttributeArray[0], FILTER_SANITIZE_STRING)."x".filter_var($specialAttributeArray[1], FILTER_SANITIZE_STRING)."x".filter_var($specialAttributeArray[2], FILTER_SANITIZE_STRING);

        return $this;
        
    }

    
    public function productToObject($row){
        $this->sku = $row['sku'];
        $this->name = $row['name'];
        $this->price = $row['price'];
        $this->productType = $row['product_type'];
        $this->specialAttribute = $row['product_special_attr'] . "Cm";

        return $this;
    }
}

?>