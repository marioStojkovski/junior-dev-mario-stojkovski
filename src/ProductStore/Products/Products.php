<?php

namespace ProductStoreApp\ProductStore\Products;
use ProductStoreApp\ProductStore\ProductStore;


abstract class Products extends ProductStore {
    public $sku;
    public $name;
    public $price;
    public $productType;
    public $specialAttribute;
    
    abstract public function productToSetAsObject($sku, $name, $price, $productType, $specialAttributeArray);

    abstract public function productToObject($array);
}

?>