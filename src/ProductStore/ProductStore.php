<?php

namespace ProductStoreApp\ProductStore;

class ProductStore {
    
    public function callClassFromProducts($class){
        $dir = 'ProductStoreApp\ProductStore\Products';  
        $classToCall =  ucfirst(strtolower($class));
        $dir .= '\\' . $classToCall . "\\" . $classToCall;
        $class = new $dir;
        return $class;
    }
}

?>
