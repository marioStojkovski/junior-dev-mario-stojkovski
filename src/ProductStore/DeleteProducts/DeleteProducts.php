<?php

namespace ProductStoreApp\ProductStore\DeleteProducts;
use ProductStoreApp\ProductStore\ProductStore;

class DeleteProducts extends ProductStore{

    private function deleteQuery($connection, $sku){
        $result = mysqli_query($connection, "DELETE FROM products WHERE sku = '$sku';");
    }

    public function deleteProducts($connection, $productsToDelete){
        foreach($productsToDelete as $product){
                $this->deleteQuery($connection, $product);
            }
    }
}

?>