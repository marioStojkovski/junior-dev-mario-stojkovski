<?php

namespace ProductStoreApp\ProductStore\GetProducts;
use ProductStoreApp\ProductStore\ProductStore;
use ProductStoreApp\ProductStore\Products\Book\Book;
use ProductStoreApp\ProductStore\Products\Furniture\Furniture;
use ProductStoreApp\ProductStore\Products\Dvd\Dvd;



class GetProducts extends ProductStore{
    private function getProducts($connection){
        $query = "SELECT * FROM products;";
        $result = mysqli_query($connection, $query);
        $arrayToReturn = [];

        if(!$result){
         die('Invalid query: ' . mysqli_error($connection));
        }

        while($row = mysqli_fetch_assoc($result)){

            $class = $this->callClassFromProducts($row['product_type']);
      
            $arrayToReturn[] = $class->productToObject($row);
        }

        mysqli_free_result($result);
        return $arrayToReturn;
    }

    public function printProducts($connection){
        $products = $this->getProducts($connection);
        
        foreach($products as $product){
            echo "item ={
                sku:'" . $product->sku . "' , name: '" .$product->name . "' , productType: '" . $product->productType . "' , price: '$" .$product->price  . "' , special: '" .$product->specialAttribute . "'},";
        }

    }
}

?>
