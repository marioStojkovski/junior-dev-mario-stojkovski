<?php

namespace ProductStoreApp\ProductStore\AddProduct;
use ProductStoreApp\ProductStore\ProductStore;
use ProductStoreApp\ProductStore\Products\Book\Book;
use ProductStoreApp\ProductStore\Products\Furniture\Furniture;
use ProductStoreApp\ProductStore\Products\Dvd\Dvd;



class AddProduct extends ProductStore {
    private function setProduct($connection, $product){
        $sku = $product->sku;
        $name= $product->name;
        $price = $product->price;
        $productType = $product->productType;
        $specialAttr = $product->specialAttribute;


        $result = mysqli_query($connection, "INSERT INTO products (sku, name, price, product_type, product_special_attr) VALUES ('$sku', '$name', '$price', '$productType', '$specialAttr')");  

    }
    

    public function addProduct($connection, $data){

        $class = $this->callClassFromProducts($data['productType']);
        $product = $class->productToSetAsObject($data['sku'], $data['name'], $data['price'], $data['productType'], $data['specialAttribute']);
        //print_r($product);
        $this->setProduct($connection, $product);
    }

}

?>