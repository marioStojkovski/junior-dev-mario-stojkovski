<?php

namespace ProductStoreApp\Connection;
use ProductStoreApp\Database\Database;


class Connection extends Database {
    public static $connection;
    public static function connect() {
        self::$connection = mysqli_connect (self::$host, self::$user, self::$pass, self::$name);
        if(mysqli_connect_errno()){
        die( "Sorry! There seems to be a problem connecting to our database.");
        
        }
        return self::$connection;
    }
}


?>