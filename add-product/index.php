<?php

use ProductStoreApp\Connection\Connection;
use ProductStoreApp\ProductStore\AddProduct\AddProduct;

require_once realpath("../vendor/autoload.php");

if(isset($_POST['addProduct'])){

    $connection = Connection::connect();
    $addProduct = new AddProduct;
    $addProduct->addProduct($connection, $_POST);

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List product page</title>
    <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
  integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css"
  integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

  <link rel="stylesheet" href="../style.css">
<!-- Latest compiled and minified JavaScript -->
<script src="../jquery-3.6.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
  integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
  crossorigin="anonymous"></script>

</head>
<body><form action="" method="post" id="product_form">
    <header class="container">
        <div class="row">
            <div class="header">
                <h1 class="col-sm-8">
                    Add product
                </h1>
                <div class="header-buttons	col-sm-4">
                    <input type="submit" name="addProduct" id="add-product" value="Save">
                    
                    <a href="../">Cancel</a>
                </div>
            </div>
        </div>
        <hr>
    </header>

    <main class="container">
        <div class="row">
        <div class="warning" class="hidden col-sm-12"></div>
            <div class="col-sm-5">

                <div>
                    <div class="input-wrapper">
                        <label for="sku">SKU</label>
                        <input type="text" name="sku" id="sku" -required>
                    </div>
                    
                    <div class="input-wrapper">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" -required>
                    </div>

                    <div class="input-wrapper">
                        <label for="price">Price ($)</label>
                        <input type="number" name="price" id="price" step=".01" -required>
                    </div>
                    
                    <div class="input-wrapper">
                        <label for="productType">Type Switcher</label>
                        <select class="selection" name="productType" id="productType" required>
                            <option selected disabled>Choose one</option>
                            <option class="selection" value="DVD" data-option='0'>DVD</option>
                            <option class="selection" value="Book" data-option='1'>Book</option>
                            <option class="selection" value="Furniture" data-option='2'>Furniture</option>
                        </select>
                    </div>
                </div>

                <div id="specific-form"></div>
            </div>
        </div>
    </main>

    <footer class="container">
        <hr>
        <div class="row">
            <small class="col-sm-12 text-center">Scandiweb test assigment</small>
        </div>
    </footer>

    <script>$(function(){

        function AddProduct(){
            this.children = ['DVD','Book','Furniture'];

            this.html = [
                `                    <div id="DVD" class="input-wrapper">
                        <label for="size">Size (MB)</label>
                        <input type="number" name="specialAttribute[]" id="size">
                        </div>
                        <h4>Please, provide size</h4>
                    `,

                    `                    <div id="Book" class="input-wrapper">
                        <label for="weight">Weight (KG)</label>
                        <input type="number" name="specialAttribute[]" id="weight">
                        </div>
                        <h4>Please, provide weight</h4>
                    `,

                    `   <div id="Furniture" >
                            <div class="input-wrapper">
                                <label for="height">Height (CM)</label>
                                <input type="number" name="specialAttribute[]" id="height">
                            </div>
                        
                            <div class="input-wrapper">
                                <label for="width">Width (CM)</label>
                                <input type="number" name="specialAttribute[]" id="width">
                            </div>
                        
                            <div class="input-wrapper">
                                <label for="length">Length (CM)</label>
                                <input type="number" name="specialAttribute[]" id="length">
                            </div>
                        <h4>Please, provide dimensions</h4>
                    </div>   `]

                    
                    ;
            

            this.showSpecificForm = function(productType){

                $('#specific-form').html(this.html[productType]);

            }   
        }

    const addingApp = new AddProduct();
    
    $(document).ready(function() {
                $('form').submit(function() {
                var incomplete = $('form :input').filter(function() {
                return $(this).val() == '';
                });
                if(incomplete.length) {
                    $('.warning').html('<h2>Please, submit required data</h2>');
                    return false;
                    }
                });
            });


    $(document).ready(function(){
            $("select.selection").change(function(){
                productType = $(this).children("option:selected").attr('data-option');
                addingApp.showSpecificForm(productType)
                
            });
    });

    })
    </script>

</form>
</body>
</html>