<?php

use ProductStoreApp\ProductStore\ProductStore;
use ProductStoreApp\Connection\Connection;
use ProductStoreApp\ProductStore\GetProducts\GetProducts;
use ProductStoreApp\ProductStore\DeleteProducts\DeleteProducts;

require_once realpath("vendor/autoload.php");

$connection = Connection::connect();

$getProducts = new GetProducts;

$deleteProducts = new DeleteProducts;

if(isset($_POST['massDelete']) && isset($_POST['toMassDelete'])){
    $deleteProducts->DeleteProducts($connection, $_POST['toMassDelete']);
}



?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List product page</title>
    <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
  integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css"
  integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">


<link rel="stylesheet" href="style.css">

<!-- Latest compiled and minified JavaScript -->
<script src="jquery-3.6.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
  integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
  crossorigin="anonymous"></script>
</head>
<body><form action="" method="post">
    <header class="container">
        <div class="row">
            <div class="header">
                <h1 class="col-sm-8">
                    Product List
                </h1>
                <div class="header-buttons	col-sm-4">
                    <a href="add-product/">ADD</a>
                    
                    <input type="submit" name="massDelete" id="mass-delete" value="MASS DELETE">
                    
                    
                </div>
            </div>
        </div>
        <hr>
    </header>

    <main class="container" id="items"></main>

    <footer class="container">
        <hr>
        <div class="row">
            <small class="col-sm-12 text-center">Scandiweb test assigment</small>
        </div>
    </footer>

    <script>$(function(){
    function ProductPage() {
        this.items = [
        
            <?php $getProducts->printProducts($connection); ?>
            
        ];
        this.listToDelete = [];
        this.printItems = function(){
           itemsPerRow = 4;
           itemNumberInRow = 0;
           productTypes = ["DVD", "Book", "Furniture"];
           productTypeBeforeTextOptions = ["Size: ", "Weight: ", "Dimensions: "];
           html = '';
           html += '<div class="row">';
           for(let item of this.items){
                productTypeTextID = jQuery.inArray( item.productType , productTypes )
                html+= `<div class="col-sm-3" >
                        <div class="panel panel-default text-info" > 
                            <div class="panel-body h4" style="overflow: hidden;" data-item="${item.sku}"> 
                                <input name="toMassDelete[]" value="${item.sku}" class="delete-checkbox" type="checkbox" id="">
                                <div class="col-sm-12 text-center">
                                    <h3>${item.sku}</h3>
                                    <h3>${item.name}</h3>
                                    <h3>${item.price}</h3>
                                    <h3>${productTypeBeforeTextOptions[productTypeTextID]}${item.special}</h3>
                                 </div>
                            </div>
                        </div>
                        </div>`
           }
           html += '</div>';
           $('#items').html(html); 
        }

        this.toggleItemToDelete = function(sku) {
           
            if(this.listToDelete.includes(sku)){
                
                for(i=0; i<this.listToDelete.length ; i++){
                    
                    if(sku == this.listToDelete[i]){
                        this.listToDelete.splice(i, 1);
                        i--;
                    }
                }
            }else {
                this.listToDelete.push(sku);
            }
        }
    }

    const app = new ProductPage();

    app.printItems();

    $(document).on("click", ".delete-checkbox", function(e){
        sku = $(this).parents( "div" ).attr("data-item");
        console.log(sku);
        app.toggleItemToDelete(sku);
    })


})</script>
</form>
</body>

</html>